const module = 'koboldworks-turn-announcer',
	missedKey = 'missedTurn';

var foundry07Compatibility = false;
Hooks.once('init', () => {
	foundry07Compatibility = game.data.version.split('.', 3)[1] === '7';
	if (foundry07Compatibility) console.log("TURN ANNOUNCER | Compatibility Mode | Foundry 0.7.x");
});

const template = `modules/${module}/template/turn-announcer.hbs`;
Hooks.once('init', async () => await loadTemplates([template]));

const secretNumber = 57; // meaningless number

var lastCombatant, currentCombatant, lastCombatantSpoke = false;

function compressMessage(cm, jq) {
	const ms = jq.find('.koboldworks.turn-announcer');
	if (ms.length === 0) return;

	const sc = jq.closest('.chat-message');
	const wh = sc.find('.whisper-to');
	sc.attr({ title: wh.text() });
	wh.remove();
	const mt = sc.find('.message-metadata');
	mt.find('.message-timestamp').hide();

	const sender = sc.find('.message-sender');
	const msgb = $('<div>').addClass('missed-token');
	sender.parent().prepend(msgb);
	msgb.append(
		ms.find('.missed-turn-icon').first(),
		$('<span>').addClass('missed-turn-name').text(sender.text()),
		ms.find('.missed-turn-text').first()
	);

	sc.addClass('missed-turn-announcer');

	// Fix spacing
	msgb.find('span').prepend('&nbsp;');
	// Remove things to enforce formatting
	sender.hide();
	sc.find('.message-header').removeClass('message-header');
	jq.find('.message-content').empty();
}

function chatMessageEvent(cm, jq, _options) {
	if (!game.user.isGM) return;

	if (cm.data.flags?.[module]?.turnAnnounce === secretNumber) {
		// NOP
	}
	else {
		if (cm.data.flags?.[module]?.missedTurn)
			compressMessage(cm, jq);
		else if (cm.data.speaker?.token === lastCombatant?.token?.id)
			lastCombatantSpoke = true;
	}
}

var lastMissedTurn, lastReported = {};

async function missedTurn(data) {
	/*
	if (lastMissedTurn?.token?.id === data.last?.token?.id) return; // last one
	lastMissedTurn = data.last;
	*/

	if (data.lastSpoke) return;
	if (data.last?.token?.id === data.combatant?.token?.id) return;

	if (lastReported.id === data.last?.token?.id && data.combat.round === lastReported.round && lastReported.combat === data.combat.id)
		return;
	lastReported = { id: data.last?.token?.id, round: data.combat.round, combat: data.combat.id };

	const msgData = {
		content: `<div class="koboldworks turn-announcer"><span class='missed-turn-icon'><i class="fas fa-exclamation-triangle"></i></span>... <span class='missed-turn-text'>missed a turn!?!</span></div>`,
		rollMode: 'selfroll',
		whisper: [...game.users.filter(u => u.isGM)],
		speaker: {
			scene: data.combat.scene,
			actor: data.last?.actor?.id,
			token: data.last?.token?.id,
			alias: data.last?.name
		},
		flags: {},
		user: game.user.id,
		//type: CONST.CHAT_MESSAGE_TYPES.OOC
	};

	msgData.flags[module] = { missedTurn: true, token: data.last };

	return ChatMessage.create(msgData);
}

async function printCard(data) {
	const defeated = foundry07Compatibility ? data.combatant.defeated : data.combatant.data.defeated;
	/*
	if (!lastCombatantSpoke && !defeated && game.settings.get(module, missedKey)) {
		try {
			await missedTurn(data);
		}
		catch (err) {
			console.error(err);
		}
	}
	*/

	if (defeated) return; // undesired
	if (data.last === data.combatant.token.id) return; // don't report the same thing multiple times

	const getUsers = (thing, permission) => Object.entries(thing.data.permission).filter(u => u[1] >= permission).map(u => u[0]);

	const msgData = {
		content: await renderTemplate(template, data),
		speaker: ChatMessage.getSpeaker({ token: foundry07Compatibility ? data.token : data.token.document, actor: data.actor }),
		rollMode: !data.unseen ? 'roll' : 'gmroll',
		whisper: !data.unseen ? [] : getUsers(data.actor, CONST.ENTITY_PERMISSIONS.OBSERVER),
		flags: {},
	};

	msgData.flags[module] = { turnAnnounce: secretNumber, token: data.token.id, round: data.round, turn: data.turn, combat: data.combat.id };

	return ChatMessage.create(msgData);
}

function announceTurn(combat, update, _options, _userId) {
	currentCombatant = combat.combatant;
	const lastC = lastCombatant;
	let lastCombatantSpokeC = false;
	try {
		if (!game.user.isGM) return;
		if (!combat.data.active) return;
		if (!combat.started) return; // undesired.
	}
	finally {
		const defeated = foundry07Compatibility ? currentCombatant?.defeated : currentCombatant?.data.defeated;
		if (!defeated) {
			lastCombatant = currentCombatant;
			lastCombatantSpokeC = lastCombatantSpoke;
			lastCombatantSpoke = false;
		}
	}

	// only continue with first GM in the list
	if (game.users.filter(o => o.isGM && o.active).sort((a, b) => b.role - a.role)[0].id !== game.user.id) return;

	const token = currentCombatant?.token;
	if (token === undefined) {
		console.warn("TURN ANNOUNCER | Combatant has no token.");
		return;
	}

	const data = {
		actor: currentCombatant.actor,
		combatant: currentCombatant,
		combat,
		last: lastC,
		lastSpoke: lastCombatantSpokeC,
		token: canvas.tokens.get(foundry07Compatibility ? token._id : token.id),
		round: combat.round,
		turn: combat.turn,
		player: currentCombatant.players[0]?.name ?? 'GM',
		get hidden() { return this.combatant?.hidden ?? false; },
		get visible() { return this.combatant?.visible ?? false; },
		get unseen() { return this.hidden || !this.visible }
	}

	printCard(data);
}

Hooks.once('init', () => {
	/*
	game.settings.register(module, missedKey, {
		name: 'Koboldworks.TurnAnnouncer.MissedTurnLabel',
		hint: 'Koboldworks.TurnAnnouncer.MissedTurnHint',
		type: Boolean,
		config: true,
		scope: 'world',
		default: true,
		onChange: (value) => value ? Hooks.on('renderChatMessage', chatMessageEvent) : Hooks.off('renderChatMessage', chatMessageEvent)
	})
	*/
});

Hooks.on('renderChatMessage', chatMessageEvent);
Hooks.on('updateCombat', announceTurn);
